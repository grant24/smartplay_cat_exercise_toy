import sys
import time
import smtplib
import RPi.GPIO as io
from gpiozero import PWMOutputDevice
from hx711 import HX711
from led import led_display
from threading import Thread


def clean_and_exit():
	print("\ncleaning...")
	io.cleanup()
	print("done.")
	print("sending email...")
	avg_pull = total_grams / total_tugs
	data = (total_tugs, avg_pull)

	server = smtplib.SMTP_SSL('smtp.gmail.com', 465)
	server.login("gregant97", "redacted")
	server.sendmail(
		"gregant97@gmail.com", 
  		"gregant97@gmail.com", 
  		"Smart-Play Usage Stats: \n\nTotal Interactions: %d\nAverage Pull Force (g): %d\n\nThis message sent with Python." % data)
	server.quit()	

	sys.exit()

# gpio pin connections
io.setmode(io.BCM)
hx = HX711(5, 6)

# initializing reading formats
hx.set_reading_format("MSB", "MSB")
hx.set_reference_unit(94)
hx.reset()
hx.tare()

# setting up load cell
print("ready for weight...")

# initialize motors
forward = PWMOutputDevice(20, True, 0, 1000)
reverse = PWMOutputDevice(21, True, 0, 1000)

# counter for pulls and running total of grams pulled for getting average
total_tugs = 0
total_grams = 0

# getting most consistent weight
def get_weight():
	val_1 = hx.get_weight(5)
	print(val_1)
	hx.power_down()
	hx.power_up()
	time.sleep(0.1)
	return val_1

# spin forward
def forward_spin():
	forward.value = 0.85
	reverse.value = 0.0
	hx.tare()

# spin backward
def backward_spin():
	forward.value = 0.0
	reverse.value = 0.85
	hx.tare()

# spin forward slow
def forward_spin_slow():
	forward.value = 0.58
	reverse.value = 0.0

# spin backward slow
def backward_spin_slow():
	forward.value = 0.0
	reverse.value = 0.58

# stop spin
def stop():
	forward.value = 0.0
	reverse.value = 0.0	

# weight while not moving
def stop_weight():
	
	global total_tugs
	global total_grams	

	curr = time.time()
	while time.time() - curr < 10:
		grams = get_weight()
		if grams > 100:
			total_tugs += 1
			total_grams += grams
			return True
	return False

# weight while moving for time period
def move_weight():

	global total_tugs
	global total_grams

	curr = time.time()
	stop()
	forward.value = 1.0
	while time.time() - curr < 1:
		grams = get_weight()
		if grams > 100:
			stop()
			total_tugs += 1
			total_grams += grams
			return True
	stop()
	return False

# incentivize: jiggle and wait on loop
def incentivize():

	global total_tugs
	global total_grams
		
	# spin slowly to attract cat
	stop()
	forward_spin_slow()	
	jig_time = time.time()
	motor_direction = True	
	jig_count = 0
	shake_count = 0

	while True:

		try:

			# timer to jiggle rope
			if time.time() - jig_time > 0.12:
				if motor_direction:
					stop()
					backward_spin_slow()
				else:
					stop()
					forward_spin_slow()
				motor_direction = not motor_direction

				jig_count += 1
				jig_time = time.time()	

			# cooldown after 4 jigs
			if jig_count == 4:
				stop()
				shake_count += 1

				# move toy to different position if shook 10 times, else execute stop_weight()
				if shake_count == 6:
					if stop_weight():
						time.sleep(15)
						while get_weight() > 100:
							print("waiting for release...")
						print("moving forward (3)...")
						forward_spin()	
						break
					print("moving toy to different position...")
					if move_weight():
						time.sleep(15)
						while get_weight() > 100:
							print("waiting for release...")	
						print("moving forward (2)...")
						forward_spin()
						break
					shake_count = 0
				if stop_weight():
					time.sleep(15)
					while get_weight() > 100:
						print("waiting for release...")
					print("moving forward (1)...")
					forward_spin()
					break

				if motor_direction:
					forward_spin_slow()
				else:
					backward_spin_slow()

				jig_count = 0
				jig_time = time.time()				

		except (KeyboardInterrupt, SystemExit):
			clean_and_exit()


# main function
def main():

	global total_tugs
	global total_grams

	# start incentivize
	motor_direction = False
	print("incentivize...")
	incentivize()		

	# setting timer
	start_time = time.time()

	# loop until user stops program
	while True:

		try:	
		
			# test if it exceeds pull threshold, reverse direction
			grams = get_weight()
			if grams > 100:
				if motor_direction is False:
					stop()
					time.sleep(15)
					while get_weight() > 100:
						print("waiting for release...")	
					print("spinning backwards...")
					backward_spin()
				else:
					stop()
					time.sleep(15)
					while get_weight() > 100:
						print("waiting for release...")	
					print("spinning forwards...")
					forward_spin()
				
				# invert motor_direction for next weight impulse, reset timer
				motor_direction = not motor_direction
				start_time = time.time()	
				total_tugs += 1
				total_grams += grams				
		
			curr_time = time.time()
			# 1 minute passes, go back to incentivize stage
			if (curr_time - start_time) > 30:
				print("incentivize...")
				incentivize()			
				start_time = time.time()		
					
		except (KeyboardInterrupt, SystemExit):
			clean_and_exit()

# led display
def display():
	global total_tugs
	global total_grams
	ld = led_display()
	ld.default_clock()
	state = 0
	while True:
		print("changing time...")
		state += 1
		if state == 3:
			state = 0
		if state == 0:
			ld.default_clock()
		elif state == 1:
			ld.tug_counter(total_tugs)
		elif state == 2:
			if total_tugs == 0:
				avg = 0
			else:
				avg = total_grams / total_tugs
			ld.pull_avg(int(avg))

# run program
if __name__ == "__main__":
	Thread(target = main).start()
	Thread(target = display).start()
