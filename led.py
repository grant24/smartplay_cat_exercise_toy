import RPi.GPIO as GPIO
import time
GPIO.setmode(GPIO.BCM)

class led_display:
	
	def __init__(self):
		self.segments = segments = (11,4,23,8,7,10,18,25,12)
		for segment in segments:
    			GPIO.setup(segment, GPIO.OUT)
    			GPIO.output(segment, 0)
		self.digits = digits = (22,27,17,24,16)
		for digit in digits:
		    GPIO.setup(digit, GPIO.OUT)
		    GPIO.output(digit, 1) 
		self.num = {' ':(0,0,0,0,0,0,0),
			'0':(1,1,1,1,1,1,0),
    			'1':(0,1,1,0,0,0,0),
    			'2':(1,1,0,1,1,0,1),
    			'3':(1,1,1,1,0,0,1),
   			'4':(0,1,1,0,0,1,1),
    			'5':(1,0,1,1,0,1,1),
    			'6':(1,0,1,1,1,1,1),
    			'7':(1,1,1,0,0,0,0),
    			'8':(1,1,1,1,1,1,1),
    			'9':(1,1,1,1,0,1,1)}
		self.interval = 10
 
	def default_clock(self):	
		curr_time = time.time()
		while time.time() - curr_time < self.interval:
			n = time.ctime()[11:13]+time.ctime()[14:16]
			s = str(n).rjust(4)
			for digit in range(4):
				for loop in range(0,7):
					GPIO.output(self.segments[loop], self.num[s[digit]][loop])
					if (int(time.ctime()[18:19])%2 == 0) and (digit == 1):
						GPIO.output(16, 1 )
					else:
						GPIO.output(16, 0)
				GPIO.output(self.digits[digit], 0)
				time.sleep(0.001)
				GPIO.output(self.digits[digit], 1)
	  			
	def tug_counter(self, num_tugs):
		curr_time = time.time()
		while time.time() - curr_time < self.interval:
			s = str(num_tugs).rjust(4)
			for digit in range(4):
				for loop in range(0, 7):
					GPIO.output(self.segments[loop], self.num[s[digit]][loop])
				GPIO.output(self.digits[digit], 0)
				time.sleep(0.001)
				GPIO.output(self.digits[digit], 1)		

	def pull_avg(self, avg):
		curr_time = time.time()
		while time.time() - curr_time < self.interval:
			s = str(avg).rjust(4)
			for digit in range(4):
				for loop in range(0, 7):
					GPIO.output(self.segments[loop], self.num[s[digit]][loop])
				GPIO.output(self.digits[digit], 0)
				time.sleep(0.001)
				GPIO.output(self.digits[digit], 1)		

		
