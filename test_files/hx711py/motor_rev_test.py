import sys
import time
import RPi.GPIO as io
from gpiozero import PWMOutputDevice
from hx711 import HX711

def clean_and_exit():
	print("\ncleaning...")
	io.cleanup()
	print("done.")
	sys.exit()

# gpio pin connections
hx = HX711(5, 6)

# initializing reading formats
hx.set_reading_format("MSB", "MSB")
hx.set_reference_unit(92)

# setting up load cell
hx.reset()
hx.tare()
print("ready for weight...")

# initialize motors
forward = PWMOutputDevice(20, True, 0, 1000)
reverse = PWMOutputDevice(21, True, 0, 1000)

# spin forward
def forward_spin():
	forward.value = 0.9
	reverse.value = 0.0

# spin backward
def backward_spin():
	forward.value = 0.0
	reverse.value = 0.9

# stop spin
def stop():
	forward.value = 0.0
	reverse.value = 0.0	


# incentivize
def incentivize():
		
	# spin slowly to attract cat
	stop()
	forward.value = 0.54	
		
	while True:

		try:
			
			# take weight until impulse felt
			val_1 = hx.get_weight(5)
			hx.power_down()
			hx.power_up()
			time.sleep(.1)
			val_2 = hx.get_weight(5)
			hx.power_down()
			hx.power_up()
			time.sleep(.1)
			print(abs(val_1 - val_2))
			
			# break loop and return to regular speed
			if abs(val_1 - val_2) > 100:
				stop()
				time.sleep(5)
				print("moving forward...")
				forward_spin()
				break

		except (KeyboardInterrupt, SystemExit):
			clean_and_exit()


# main function
def main():

	# start incentivize
	motor_direction = False
	print("incentivize...")
	incentivize()		

	# setting timer
	start_time = time.time()

	# loop until user stops program
	while True:

		try:	

			# get impulse and print it
			val_1 = hx.get_weight(5)
			hx.power_down()
			hx.power_up()
			time.sleep(.1)
			# val_2 = hx.get_weight(5)
			# hx.power_down()
			# hx.power_up()
			# time.sleep(.1)
			# print(abs(val_1 - val_2))
			print(val_1)
		
			# test impulse if it exceeds pull threshold, reverse direction
			# if abs(val_1 - val_2) > 100:
			if val_1 > 100:
				if motor_direction is False:
					stop()
					time.sleep(5)
					print("spinning backwards...")
					backward_spin()
				else:
					stop()
					time.sleep(5)
					print("spinning forwards...")
					forward_spin()
				
				# invert motor_direction for next weight impulse
				motor_direction = not motor_direction
				start_time = time.time()	
			
			curr_time = time.time()
			# 2min pass, go back to incentivze stage
			if (curr_time - start_time) > 10:
				print("incentivize...")
				incentivize()			
				start_time = time.time()
								
		except (KeyboardInterrupt, SystemExit):
			clean_and_exit()

# run program
if __name__ == "__main__":
	main()
