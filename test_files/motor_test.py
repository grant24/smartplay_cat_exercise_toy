import sys
import time
import RPi.GPIO as GPIO


Forward = 22
Backward = 24
sleeptime = 1


GPIO.setmode(GPIO.BOARD)

GPIO.setup(Forward, GPIO.OUT)
GPIO.setup(Backward, GPIO.OUT)

print("First direction.")
GPIO.output(Forward, GPIO.HIGH)
time.sleep(20)
GPIO.output(Forward, GPIO.LOW)

print("Second direction.")
GPIO.output(Backward, GPIO.HIGH)
time.sleep(20)
GPIO.output(Backward, GPIO.LOW)

GPIO.cleanup()
