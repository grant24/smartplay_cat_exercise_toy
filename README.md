# Smart-Play Cat Exercise Toy

Prototype of Smart-Play Cat Exercise Toy

[Python 3.4 on Raspberry Pi (Raspbian)]

Libraries: https://github.com/tatobari/hx711py  |  https://github.com/gpiozero/gpiozero  |  https://github.com/metachris/RPIO

**** Driver file at hx711py/motor_rev_test.py ****

run with command: >python3 exec.py
must have Python 3 installed and be within the hx711py directory


Demo: https://photos.app.goo.gl/v95vzScnHqAan4Cx8